package com.kg.crud_api_rest_spring.controllers;

import com.kg.crud_api_rest_spring.models.UserModel;
import com.kg.crud_api_rest_spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping()
    public ArrayList<UserModel> getUsers(){
        return userService.getUsers();
    }
    @PostMapping()
    public UserModel newUser(@RequestBody UserModel user){
        return userService.newUser(user);
    }
    @GetMapping(path = "/{id}")
    public Optional getUserById(@PathVariable("id") Long id){
        return userService.selectUser(id);
    }
    @GetMapping("query")
    public ArrayList<UserModel> getByLevel(@RequestParam("level") Integer level){
        return userService.findByLevel(level);
    }
    @DeleteMapping(path = "{id}")
    public String deleteById(@PathVariable("id") Long id){
        return userService.deleteUser(id)? "Usuario Eliminado" : "No se pudo eliminar";
    }
}
