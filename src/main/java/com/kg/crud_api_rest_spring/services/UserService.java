package com.kg.crud_api_rest_spring.services;

import com.kg.crud_api_rest_spring.models.UserModel;
import com.kg.crud_api_rest_spring.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public ArrayList<UserModel> getUsers(){
       return (ArrayList<UserModel>) userRepository.findAll();
    }
    public UserModel newUser(UserModel user){
        return userRepository.save(user);
    }
    public Optional<UserModel> selectUser(Long id){
        return userRepository.findById(id);
    }
    public ArrayList<UserModel> findByLevel(Integer level){
        return userRepository.findByLevel(level);
    }
    public Boolean deleteUser(Long id){
        try{
            userRepository.deleteById(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
