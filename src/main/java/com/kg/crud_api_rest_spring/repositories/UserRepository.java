package com.kg.crud_api_rest_spring.repositories;

import com.kg.crud_api_rest_spring.models.UserModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UserRepository extends CrudRepository<UserModel,Long> {
    public abstract ArrayList<UserModel> findByLevel(Integer level);
}
